This repository contains my changes to the [benchmarksgame][bmg-website] code [repository][bmg-cvs] to include Kotlin tests.

It is a fork of [Sebastian Thiel's mirror][byron-cvs] of the CVS repository

# Why ?

I want to test the performance of Kotlin versus Java, and potentially other languages.  

# How ?

To get these tests running with Kotlin you need to make sure to add the Kotlin
Command Line Tools, which can be found [here][kotlin-cmdline-tools].

[kotlin-cmdline-tools]: https://kotlinlang.org/docs/tutorials/command-line.html
[byron-cvs]: https://github.com/Byron/benchmarksgame-cvs-mirror
[bmg-cvs]: https://alioth.debian.org/scm/?group_id=100815
[bmg-website]: http://benchmarksgame.alioth.debian.org
[issues]: https://github.com/Byron/benchmarksgame-cvs-mirror/issues
[first bug]: https://github.com/Byron/benchmarksgame-cvs-mirror/issues/1#issuecomment-292712784
